Name : Graciela Chika Putri

NPM : 1706103436

Class : A

Notes:

1. Find a reason why you should use git branches:
Git branch digunakan untuk me-maintain fitur-fitur pada project.
Fitur yang akan di launch dan yang ready akan masuk ke branch master.
Sedangkan fitur yang masih tahap pengembangan di push pada branch lain sebelum di merge ke branch master.

2. Describe how you use git branches on that repository:
Menggunakan command "git branch" kemudian "git checkout"
Kemudian push file baru ke branch tersebut.
Kemudian checkout ke branch master lalu merge dengan branch baru yang sudah dibuat.

3. Find a scenario so you should use git revert:
Ketika terdapat fitur dalam project yang ingin dihapus atau diubah dari live project.

4. Describe how you use git revert on that repository:
Menggunakan command "git log" untuk mendapatkan hash commit
Kemudian menggunakan sintaks "git revert"